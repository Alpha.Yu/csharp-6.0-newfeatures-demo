using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
using static System.Math;
using static System.DayOfWeek;

namespace csharp_6_0_newfeatures_demo
{
    class Program
    {
        //01 nameof
        /*
        private static void Func1(int x) { }
        private string F<T>() => nameof(T);
        private void Func2(string msg) { }

        static void Main(string[] args)
        {
            var program = new Program();
            Console.WriteLine(nameof(csharp_6_0_newfeatures_demo));
            Console.WriteLine(nameof(Func1));
            Console.WriteLine(nameof(Program));
            Console.WriteLine(nameof(program));
            Console.WriteLine(nameof(F));
            Console.Read();
        }
        */

        //02 Interpolated Strings
        /*
        static void Main(string[] args)
        {
            var name = "Alpha";
            Console.WriteLine($"Hello, {name}");

            var s1 = $"hello, {name}";
            Console.WriteLine(s1.ToString());

            IFormattable s2 = $"Hello, {name}";
            Console.WriteLine(s2);
            Console.WriteLine(s2.ToString());

            FormattableString s3 = $"Hello, {name}";
            Console.WriteLine(s3);
            Console.WriteLine(s3.ToString());

            var s4 = $"hello,{{name}}";
            Console.WriteLine(s4);

            var s5 = $"hello,{{{name}}}";
            Console.WriteLine(s5);

            Console.Read();
        }
        */

        //03 Null-Conditional Operator
        /*
        static void Main(string[] args)
        {
            string name = null;
            Console.WriteLine($"1：{name?.Length}");

            name = "Alpha";
            Console.WriteLine($"2：{name?.Length}");
            Console.WriteLine($"3: {name?[0]}");

            Func<int> func = () =>0;
            if (func != null)
            {
                func();
            }

            //简化调用
            func?.Invoke();

            Console.Read();
        }
        */

        //04 Await in catch and finally blocks
        /*
        static void Main(string[] args)
        {
            Test();
        }
        static async Task Test()
        {
            var wc = new WebClient();
            try
            {
                await wc.DownloadDataTaskAsync("");
            }
            catch (Exception)
            {
                await wc.DownloadDataTaskAsync("");　　//It is OK in C#6.0.
            }
            finally
            {
                await wc.DownloadDataTaskAsync("");　　//It is OK in C#6.0.
            }
        }
        */

        //05   Auto-Property Initializer
        /*
        class Demo
        {
            public string Name { get; set; } = "Alpha";
        }
        static void Main(string[] args)
        {
            var myDemo = new Demo();
            Console.WriteLine(myDemo.Name);
            Console.Read();
        }
        */

        //06 Getter-Only Auto-Property
        /*
        //C# 6.0
        public string Name { get; } = "Alpha";  
        //C#5.0 
        public int Age { get; private set; }
        static void Main(string[] args)
        { 

        }
        */

        //07 Expression bodies on method-like members
        /*
        class Demo
        {
            public int this[int id] => id;  //索引
            public double Add(int x, int y) => x + y;   //带返回值方法
            public void Output() => Console.WriteLine("Hi, Alpha!"); //无返回值方法
            public void Input(string s) => Console.WriteLine(s);
        }
        static void Main(string[] args)
        {
            Demo myDemo = new Demo();
            Console.WriteLine(myDemo[0]);
            Console.WriteLine(myDemo.Add(1, 2));
            myDemo.Output();
            myDemo.Input("hello world");
            Console.ReadKey();
        }
        */

        //08 Index initializers
        /*
        static void Main(string[] args)
        {
            //C# 6.0
            var nums = new Dictionary<int, string>
            {
                [7] = "seven",
                [9] = "nine",
                [13] = "thirteen"
            };
            //c# 5.0
            var otherNums = new Dictionary<int, string>()
            {
                {1, "one"},
                {2, "two"},
                {3, "three"}
            };
        }
        */

        //09 using static
        /*
        static void Main(string[] args)
        {
            //C# 6.0
            //using static System.Console;
            //using static System.Math;
            //using static System.DayOfWeek;
            WriteLine(Sqrt(3 * 3 + 4 * 4));
            WriteLine(Friday - Monday);
            //C# 5.0
            Console.WriteLine(Math.Sqrt(3 * 3 + 4 * 4));
            Console.WriteLine(DayOfWeek.Friday - DayOfWeek.Monday);

            Console.ReadKey();
        }
        */

        //10 Exception filters
        /*
        private static bool Log(Exception e) { return false; }
        static void Main(string[] args)
        {
            try
            {
                throw new ApplicationException("1");
            }
            catch (ApplicationException ex) when (ex.Message == "2")
            {
                // this one won't execute.
            }
            catch (ApplicationException ex) when (ex.Message == "1")
            {
                // this one will execute
            }
        }
        */
    }
}
